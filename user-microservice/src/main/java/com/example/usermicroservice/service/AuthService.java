package com.example.usermicroservice.service;

import com.example.usermicroservice.entity.UserEntity;

public interface AuthService {
    // Login and Signup Functions

	//Login Function
    UserEntity login(String username, String password);

    //Signup Function
    UserEntity signup(String username, String password, String email, long phone, String address);

    // Logout Function
    void logout();
}
