package com.example.usermicroservice.service;


import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.example.usermicroservice.entity.AccountEntity;
import com.example.usermicroservice.entity.UserEntity;
import com.example.usermicroservice.utilities.ACCOUNTTYPE;
import com.example.usermicroservice.utilities.Payment;

@Service
public interface AccountServices {
	public List<AccountEntity> getAllAccounts(long userId);
	public AccountEntity getSavingAccountDetails(long userId);
	public AccountEntity getCurrentAccountDetails(long userId);
	public AccountEntity getLoanAccountDetails(long userId);
	public AccountEntity depositMoney(Payment paymentObj);
	public AccountEntity withdrawMoney(Payment paymentObj);
	public AccountEntity transferMoney(Payment paymentObj);
	public ResponseEntity<AccountEntity> getUpdatedAccount(AccountEntity account,Payment paymentObj);
	public AccountEntity createUserAccount(UserEntity user,ACCOUNTTYPE type); 

}
