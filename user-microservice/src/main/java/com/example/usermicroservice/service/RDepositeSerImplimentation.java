package com.example.usermicroservice.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.usermicroservice.entity.AccountEntity;
import com.example.usermicroservice.entity.RecurringDeposite;
import com.example.usermicroservice.repo.AccountRepo;
import com.example.usermicroservice.repo.RecurringDepositeRepo;

@Service
public class RDepositeSerImplimentation implements RecurringDepositeService {
	@Autowired
	AccountRepo accountRepository;
	@Autowired
	RecurringDepositeRepo recurringDepositRepository;

	@Override
	public Optional<AccountEntity> getAccountByUserId(Long userId) {
		return accountRepository.findById(userId);
	}
	@Override
	public double calculateTotalSavings(int tenureMonths, double monthlyAmount, double interestRate) {
		double rate = interestRate / 100; // Convert percentage to decimal
		double totalAmount = 0;

		for (int i = 1; i <= tenureMonths; i++) {
			totalAmount += monthlyAmount;
			totalAmount *= (1 + rate / 12); // Compounded monthly
		}
		return totalAmount;
	}

	public RecurringDeposite createRecurringDeposit(RecurringDeposite entity) {
	 recurringDepositRepository.save(entity);
		return entity;
	}

	@Override
	public void updateAccountBalance(Long userId, double amount) {
		Optional<AccountEntity> user = accountRepository.findById(userId);
		AccountEntity useracount = user.get();
		double updatedAmount = useracount.getAmountAvailable() - amount;
		user.get().setAmountAvailable(updatedAmount);
		accountRepository.save(useracount);

	}

	
}
