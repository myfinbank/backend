package com.example.usermicroservice.service;

import com.example.usermicroservice.entity.LoanAccountEntity;

public interface LoanAccountService {
	public double calculateEMI(LoanAccountEntity loan);
    
}
