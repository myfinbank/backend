package com.example.usermicroservice.service;

import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

import javax.security.auth.login.AccountException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.example.usermicroservice.entity.AccountEntity;
import com.example.usermicroservice.entity.TransactionEntity;
import com.example.usermicroservice.entity.UserEntity;
import com.example.usermicroservice.repo.AccountRepo;
import com.example.usermicroservice.repo.TransactionRepo;
import com.example.usermicroservice.utilities.ACCOUNTTYPE;
import com.example.usermicroservice.utilities.Payment;

import jakarta.transaction.Transactional;

@Service
public class AccountServiceImpl implements AccountServices {
	@Autowired
	AccountRepo repo;

	@Autowired
	TransactionService service;

	@Autowired
	TransactionRepo trepo;

	@Override
	public AccountEntity getSavingAccountDetails(long userId) {
		AccountEntity user = repo.findByUserIdAndType(userId, ACCOUNTTYPE.SAVING);
		return user;
	}

	@Override
	public AccountEntity getCurrentAccountDetails(long userId) {
		AccountEntity user = repo.findByUserIdAndType(userId, ACCOUNTTYPE.CURRENT);
		return user;
	}

	@Override
	public AccountEntity getLoanAccountDetails(long userId) {
		AccountEntity user = repo.findByUserIdAndType(userId, ACCOUNTTYPE.LOANACCOUNT);
		return user;
	}

	@Override
	public List<AccountEntity> getAllAccounts(long userId) {
		List<AccountEntity> accountList = repo.findAllByUserId(userId);
		return accountList;
	}

	@Transactional
	@Override
	public AccountEntity depositMoney(Payment paymentObj) {
		System.out.println(paymentObj);
		AccountEntity userAccount = repo.findByAccountNumber(paymentObj.getFromAccountNumber());
		repo.updateAmountAvailable(paymentObj.getFromAccountNumber(),
				paymentObj.getAmount() + userAccount.getAmountAvailable());
		AccountEntity updatedAccount = repo.findByAccountNumber(userAccount.getAccountNumber());
		try {
			TransactionEntity transactionObj = service.transferAmount(paymentObj);
			transactionObj.setUserId(userAccount.getUserId());
			transactionObj.setRemark("Added to");
			transactionObj.setAmount(+paymentObj.getAmount());
			System.out.println(transactionObj.toString());
			trepo.save(transactionObj);
		} catch (AccountException e) {
			e.printStackTrace();
		}
		return updatedAccount;
	}

	@Transactional
	@Override
	public AccountEntity withdrawMoney(Payment paymentObj) {
		AccountEntity userAccount = repo.findByAccountNumber(paymentObj.getFromAccountNumber());
		repo.updateAmountAvailable(paymentObj.getFromAccountNumber(),
				userAccount.getAmountAvailable() - paymentObj.getAmount());
		AccountEntity updatedAccount = repo.findByAccountNumber(userAccount.getAccountNumber());
		try {
			TransactionEntity transactionObj = service.transferAmount(paymentObj);
			transactionObj.setUserId(userAccount.getUserId());
			transactionObj.setRemark("Debited from");
			transactionObj.setAmount(-paymentObj.getAmount());
			trepo.save(transactionObj);
		} catch (AccountException e) {
			e.printStackTrace();
		}
		return updatedAccount;
	}

	@Transactional
	@Override
	public AccountEntity transferMoney(Payment paymentObj) {
		System.out.println(paymentObj.toString());
		AccountEntity userFromAccount = repo.findByAccountNumber(paymentObj.getFromAccountNumber());
		repo.updateAmountAvailable(paymentObj.getFromAccountNumber(),
				userFromAccount.getAmountAvailable() - paymentObj.getAmount());
		AccountEntity userToAccount = repo.findByAccountNumber(paymentObj.getToAccountNumber());
		repo.updateAmountAvailable(paymentObj.getToAccountNumber(),
				userToAccount.getAmountAvailable() + paymentObj.getAmount());
		AccountEntity updatedAccount = repo.findByAccountNumber(userToAccount.getAccountNumber());
		try {
			TransactionEntity transactionObj = service.transferAmount(paymentObj);
			transactionObj.setUserId(userFromAccount.getUserId());
			transactionObj.setRemark("Transfer to");
			transactionObj.setAmount(-paymentObj.getAmount());
			trepo.save(transactionObj);
		} catch (AccountException e) {
			e.printStackTrace();
		}
		return updatedAccount;
	}

	@Override
	public ResponseEntity<AccountEntity> getUpdatedAccount(AccountEntity updatedAccount, Payment paymentObj) {
		if (updatedAccount != null) {
			if (paymentObj.getType().equals(ACCOUNTTYPE.SAVING)) {
				AccountEntity account = getSavingAccountDetails(updatedAccount.getUserId());
				return ResponseEntity.status(HttpStatus.CREATED).body(account);
			} else if (paymentObj.getType().equals(ACCOUNTTYPE.CURRENT)) {
				AccountEntity account = getCurrentAccountDetails(updatedAccount.getUserId());
				return ResponseEntity.status(HttpStatus.CREATED).body(account);
			} else if (paymentObj.getType().equals(ACCOUNTTYPE.LOANACCOUNT)) {
				AccountEntity account = getLoanAccountDetails(updatedAccount.getUserId());
				return ResponseEntity.status(HttpStatus.CREATED).body(account);
			} else {
				return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
			}
		}
		return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
	}
	private static final AtomicLong counter = new AtomicLong(100000000000L);
	@Override
	public AccountEntity createUserAccount(UserEntity user,ACCOUNTTYPE type) {
		AccountEntity existaccount = repo.findByUserIdAndType(user.getUserId(), type);
		if(existaccount==null) {
			
		long accountNumberLong = counter.getAndIncrement();
		String accountNumber = String.format("%012d", accountNumberLong);
		AccountEntity newaccount = new AccountEntity();
		newaccount.setAccountNumber(accountNumber);
		newaccount.setAmountAvailable(0);
		if(type.equals(ACCOUNTTYPE.SAVING)) {			
			newaccount.setType(ACCOUNTTYPE.SAVING);
		}
		else if(type.equals(ACCOUNTTYPE.CURRENT)) {
			newaccount.setType(ACCOUNTTYPE.CURRENT);
		}
		else if(type.equals(ACCOUNTTYPE.LOANACCOUNT)) {
			newaccount.setType(ACCOUNTTYPE.LOANACCOUNT);
		}
		newaccount.setUserId(user.getUserId());		
		return repo.save(newaccount);
		}
		return null;
	}

}
