package com.example.usermicroservice.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.usermicroservice.entity.UserEntity;
import com.example.usermicroservice.repo.UserRepo;
import com.example.usermicroservice.utilities.ACCOUNTTYPE;
import com.example.usermicroservice.utilities.ROLE;

@Service
public class AuthServiceImpl implements AuthService {
    @Autowired
    private UserRepo userRepo;
    
    @Autowired
    AccountServiceImpl aservice;

    public UserEntity login(String username, String password) {
        UserEntity user = userRepo.findByUsername(username);
        System.out.println(user + username + password);
        if (user == null) {
            return null;
        }
        if (!user.getPassword().equals(password)) {
            return null;
        }
        return user;
    }

    // logout
    public void logout() {
        return;
    }

    @Override
    public UserEntity signup(String username, String password, String email, long phone, String address) {

        if (!validateSignup(username, password, email, phone, address)) {
            return null;
        }

        UserEntity user = new UserEntity();
        user.setUsername(username);
        user.setPassword(password);
        user.setEmail(email);
        user.setPhone(phone);
        user.setAddress(address);
        user.setRole(ROLE.CUS);
        userRepo.save(user);
        aservice.createUserAccount(user, ACCOUNTTYPE.SAVING);
        return user;
    }

    // validate signup input
    public boolean validateSignup(String username, String password, String email, long phone, String address) {
        if (username == null || password == null || email == null || phone == 0 || address == null) {
            return false;
        }
        return true;
    }

}
