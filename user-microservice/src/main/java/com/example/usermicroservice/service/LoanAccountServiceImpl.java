package com.example.usermicroservice.service;

import org.springframework.stereotype.Service;

import com.example.usermicroservice.entity.LoanAccountEntity;

@Service
public class LoanAccountServiceImpl implements LoanAccountService{
	
	 public double calculateEMI(LoanAccountEntity loan) {
	        double principal = loan.getSancationedAmount();  // Principal Loan Amount
	        double annualRateOfInterest = loan.getRoi();   // Annual Rate of Interest
	        int tenureInMonths = loan.getPeriod();          // Loan Tenure in Months

	        // Calculate monthly interest rate
	        double monthlyInterestRate = (annualRateOfInterest / 12) / 100;

	        // Calculate EMI using the formula
	        double emi = (principal * monthlyInterestRate * Math.pow(1 + monthlyInterestRate, tenureInMonths))
	                / (Math.pow(1 + monthlyInterestRate, tenureInMonths) - 1);

	        // Update the loan entity with the calculated EMI
	        loan.setInstallment((int) emi);

	        // Save the updated loan entity (You would use a repository to save it to the database)
	        //loan = loanRepository.save(loan);

	        return emi;
	    }

}
