package com.example.usermicroservice.service;

import java.util.Date;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import javax.security.auth.login.AccountException;
import javax.security.auth.login.AccountNotFoundException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.usermicroservice.entity.AccountEntity;
import com.example.usermicroservice.entity.TransactionEntity;
import com.example.usermicroservice.repo.AccountRepo;
import com.example.usermicroservice.repo.TransactionRepo;
import com.example.usermicroservice.utilities.Payment;
import com.example.usermicroservice.utilities.STATUS;


@Service
public class TransactionService {

	@Autowired
	private TransactionRepo transactionRepo;
	@Autowired
	private AccountRepo accountRepo;

	public TransactionEntity transferAmount(Payment paymentObj) throws AccountException {
		// Check if the transactor and transactee accounts exist
		AccountEntity transactorAccount = accountRepo.findByAccountNumber(paymentObj.getFromAccountNumber());
		AccountEntity transacteeAccount = accountRepo.findByAccountNumber(paymentObj.getToAccountNumber());

		if (transactorAccount == null)
			throw new AccountNotFoundException("Account Not Found.");
		else {
			TransactionEntity response = new TransactionEntity();
			response.setTimeStamp(new Date());
			AtomicInteger counter = new AtomicInteger(1000);
			int uniqueNumber = counter.getAndIncrement();
			long timeStamp = System.currentTimeMillis();
			String transactionId = "TXN" + timeStamp + uniqueNumber;
			response.setTransactionId(transactionId);
			response.setTransactorAccountId(paymentObj.getFromAccountNumber());
			if(transacteeAccount!=null) {				
				response.setTransacteeAccountId(paymentObj.getToAccountNumber());
			}
			else {
				response.setTransacteeAccountId(paymentObj.getFromAccountNumber());
			}
			response.setType(paymentObj.getType());
			response.setStatus(STATUS.SUCCESS);
			return response;
		}
	}

	public List<TransactionEntity> getHistoryById(long userId) {
		return transactionRepo.findByUserId(userId);
	}

}
