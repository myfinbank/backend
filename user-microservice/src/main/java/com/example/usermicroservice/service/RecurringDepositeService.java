package com.example.usermicroservice.service;

import java.util.Optional;

import com.example.usermicroservice.entity.AccountEntity;
import com.example.usermicroservice.entity.RecurringDeposite;

public interface RecurringDepositeService {
	public Optional<AccountEntity> getAccountByUserId(Long userId);

	public void updateAccountBalance(Long userId, double amount) ;

	 public RecurringDeposite createRecurringDeposit(RecurringDeposite entity) ;

	double calculateTotalSavings(int tenureMonths, double monthlyAmount, double interestRate);

}
