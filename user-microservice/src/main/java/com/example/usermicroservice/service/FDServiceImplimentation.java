package com.example.usermicroservice.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.usermicroservice.entity.AccountEntity;
import com.example.usermicroservice.entity.FixedDepositEntity;
import com.example.usermicroservice.repo.AccountRepo;
import com.example.usermicroservice.repo.FixedDepositRepository;

@Service
public class FDServiceImplimentation implements FixedDepositService {
	@Autowired
	private AccountRepo accountRepository;
	@Autowired
	private FixedDepositRepository fixedDepositRepository;

	@Override
	public Optional<AccountEntity> getAccountByUserId(Long userId) {
		return accountRepository.findById(userId);
	}

	@Override
	public void updateAccountBalance(Long userId, double amount) {
		Optional<AccountEntity> user = accountRepository.findById(userId);
		AccountEntity useracount = user.get();
		double updatedAmount = useracount.getAmountAvailable() - amount;
		user.get().setAmountAvailable(updatedAmount);
		accountRepository.save(useracount);

	}

	@Override
	public double calculateMaturityAmount(int tenure, double amount, double interestRate) {
		double rate = interestRate / 100;
		double totalAmount = amount * Math.pow(1 + rate, tenure);
		return totalAmount;
	}

	@Override
	public FixedDepositEntity createFixedDeposit(FixedDepositEntity entity) {
		return fixedDepositRepository.save(entity);
	}

} 
