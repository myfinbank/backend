package com.example.usermicroservice.service;

import java.util.Optional;

import com.example.usermicroservice.entity.AccountEntity;
import com.example.usermicroservice.entity.FixedDepositEntity;

public interface FixedDepositService {
	Optional<AccountEntity> getAccountByUserId(Long userId);
    void updateAccountBalance(Long userId, double amount);
    FixedDepositEntity createFixedDeposit(FixedDepositEntity entity);
    public double calculateMaturityAmount(int tenure, double amount, double interestRate);
}
