package com.example.usermicroservice.entity;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;

@Entity
public class RecurringDeposite{
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int recurringDepositId;
    private double amount;
    private double interestRate=6.5;
    private int tenure;
    
	public RecurringDeposite(int recurringDepositId, double amount, double interestRate, int tenure) {
		super();
		this.recurringDepositId = recurringDepositId;
		this.amount = amount;
		this.interestRate = interestRate;
		this.tenure = tenure;
	}
	public RecurringDeposite() {
		super();
		// TODO Auto-generated constructor stub
	}
	public int getRecurringDepositId() {
		return recurringDepositId;
	}
	public void setRecurringDepositId(int recurringDepositId) {
		this.recurringDepositId = recurringDepositId;
	}
	public double getAmount() {
		return amount;
	}
	public void setAmount(double amount) {
		this.amount = amount;
	}
	public double getInterestRate() {
		return interestRate;
	}
	public void setInterestRate(double interestRate) {
		this.interestRate = interestRate;
	}
	public int getTenure() {
		return tenure;
	}
	public void setTenure(int tenure) {
		this.tenure = tenure;
	}
	
	
}
