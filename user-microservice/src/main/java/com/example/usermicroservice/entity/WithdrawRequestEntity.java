package com.example.usermicroservice.entity;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;

@Entity
public class WithdrawRequestEntity {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Long wRequestId;
	public int userId;
	private String userAccountNumber;
	private double amount;
	private String remark;
	public boolean isApproved;
	public WithdrawRequestEntity() {
		super();
		// TODO Auto-generated constructor stub
	}
	@Override
	public String toString() {
		return "WithdrawRequestEntity [wRequestId=" + wRequestId + ", userId=" + userId + ", userAccountNumber="
				+ userAccountNumber + ", amount=" + amount + ", remark=" + remark + ", isApproved=" + isApproved + "]";
	}
	public WithdrawRequestEntity(Long wRequestId, int userId, String userAccountNumber, double amount, String remark,
			boolean isApproved) {
		super();
		this.wRequestId = wRequestId;
		this.userId = userId;
		this.userAccountNumber = userAccountNumber;
		this.amount = amount;
		this.remark = remark;
		this.isApproved = isApproved;
	}
	public Long getwRequestId() {
		return wRequestId;
	}
	public void setwRequestId(Long wRequestId) {
		this.wRequestId = wRequestId;
	}
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public String getUserAccountNumber() {
		return userAccountNumber;
	}
	public void setUserAccountNumber(String userAccountNumber) {
		this.userAccountNumber = userAccountNumber;
	}
	public double getAmount() {
		return amount;
	}
	public void setAmount(double amount) {
		this.amount = amount;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public boolean isApproved() {
		return isApproved;
	}
	public void setApproved(boolean isApproved) {
		this.isApproved = isApproved;
	}
	
	
	

}
