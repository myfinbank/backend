package com.example.usermicroservice.entity;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;

@Entity
public class FixedDepositEntity {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int fixedDepositId;
    private double amount;
    private double interestRate=7.5;
    private int tenure;
    
	public int getFixedDepositId() {
		return fixedDepositId;
	}
	public void setFixedDepositId(int fixedDepositId) {
		this.fixedDepositId = fixedDepositId;
	}
	public double getAmount() {
		return amount;
	}
	public void setAmount(double amount) {
		this.amount = amount;
	}
	public double getInterestRate() {
		return interestRate;
	}
	public void setInterestRate(double interestRate) {
		this.interestRate = interestRate;
	}
	public int getTenure() {
		return tenure;
	}
	public void setTenure(int tenure) {
		this.tenure = tenure;
	}
	public FixedDepositEntity(int fixedDepositId, double amount, double interestRate, int tenure) {
		super();
		this.fixedDepositId = fixedDepositId;
		this.amount = amount;
		this.interestRate = interestRate;
		this.tenure = tenure;
	}
	public FixedDepositEntity() {
		super();
		// TODO Auto-generated constructor stub
	}	
}