package com.example.usermicroservice.entity;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;

@Entity
public class DepositRequestEntity {
	@Id
	public Long dRequestId;
	public int userId;
	private String userAccountNumber;
	private double amount;
	private String remark;
	public boolean isApproved;
	public DepositRequestEntity() {
		super();
		// TODO Auto-generated constructor stub
	}
	public DepositRequestEntity(Long dRequestId, int userId, String userAccountNumber, double amount, String remark,
			boolean isApproved) {
		super();
		this.dRequestId = dRequestId;
		this.userId = userId;
		this.userAccountNumber = userAccountNumber;
		this.amount = amount;
		this.remark = remark;
		this.isApproved = isApproved;
	}
	public Long getdRequestId() {
		return dRequestId;
	}
	public void setdRequestId(Long dRequestId) {
		this.dRequestId = dRequestId;
	}
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public String getUserAccountNumber() {
		return userAccountNumber;
	}
	public void setUserAccountNumber(String userAccountNumber) {
		this.userAccountNumber = userAccountNumber;
	}
	public double getAmount() {
		return amount;
	}
	public void setAmount(double amount) {
		this.amount = amount;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public boolean isApproved() {
		return isApproved;
	}
	public void setApproved(boolean isApproved) {
		this.isApproved = isApproved;
	}
	

}
