package com.example.usermicroservice.entity;

import com.example.usermicroservice.utilities.ACCOUNTTYPE;

import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;


@Entity
public class AccountEntity {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	long accountId;
	String accountNumber;
	long userId;
	double amountAvailable;
	
	@Enumerated(EnumType.STRING)
	ACCOUNTTYPE type;
	
	
	public AccountEntity() {
		super();
		// TODO Auto-generated constructor stub
	}


	public AccountEntity(String accountNumber, long userId, long amountAvailable, ACCOUNTTYPE type) {
		super();
		this.accountNumber = accountNumber;
		this.userId = userId;
		this.amountAvailable = amountAvailable;
		this.type = type;
	}


	public long getAccountId() {
		return accountId;
	}


	public void setAccountId(long accountId) {
		this.accountId = accountId;
	}


	public String getAccountNumber() {
		return accountNumber;
	}


	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}


	public long getUserId() {
		return userId;
	}


	public void setUserId(long userId) {
		this.userId = userId;
	}


	public double getAmountAvailable() {
		return amountAvailable;
	}


	public void setAmountAvailable(double amountAvailable) {
		this.amountAvailable = amountAvailable;
	}


	public ACCOUNTTYPE getType() {
		return type;
	}


	public void setType(ACCOUNTTYPE type) {
		this.type = type;
	}


	@Override
	public String toString() {
		return "AccountEntity [accountId=" + accountId + ", accountNumber=" + accountNumber + ", userId=" + userId
				+ ", amountAvailable=" + amountAvailable + ", type=" + type + "]";
	}
	
	
	
}
