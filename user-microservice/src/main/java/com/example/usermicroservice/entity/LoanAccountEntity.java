package com.example.usermicroservice.entity;

import com.example.usermicroservice.utilities.LOANTYPE;

import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Inheritance;
import jakarta.persistence.InheritanceType;

@Entity
public class LoanAccountEntity{
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int loanId;
	private double sancationedAmount;
	private double requestedAmount;
	private boolean isApproved;
	private int period;
	private int installment;
	private int roi;
	private int interestAmount;
	private double outsandingAmount;
	@Enumerated(EnumType.STRING)
	private LOANTYPE loanType;
	
	
	public LoanAccountEntity() {
		super();
		// TODO Auto-generated constructor stub
	}


	public LoanAccountEntity(int loanId, double sancationedAmount, double requestedAmount, boolean isApproved,
			int period, int installment, int roi, int interestAmount, double outsandingAmount, LOANTYPE loanType) {
		super();
		this.loanId = loanId;
		this.sancationedAmount = sancationedAmount;
		this.requestedAmount = requestedAmount;
		this.isApproved = isApproved;
		this.period = period;
		this.installment = installment;
		this.roi = roi;
		this.interestAmount = interestAmount;
		this.outsandingAmount = outsandingAmount;
		this.loanType = loanType;
	}


	public int getLoanId() {
		return loanId;
	}


	public void setLoanId(int loanId) {
		this.loanId = loanId;
	}


	public double getSancationedAmount() {
		return sancationedAmount;
	}


	public void setSancationedAmount(double sancationedAmount) {
		this.sancationedAmount = sancationedAmount;
	}


	public double getRequestedAmount() {
		return requestedAmount;
	}


	public void setRequestedAmount(double requestedAmount) {
		this.requestedAmount = requestedAmount;
	}


	public boolean isApproved() {
		return isApproved;
	}


	public void setApproved(boolean isApproved) {
		this.isApproved = isApproved;
	}


	public int getPeriod() {
		return period;
	}


	public void setPeriod(int period) {
		this.period = period;
	}


	public int getInstallment() {
		return installment;
	}


	public void setInstallment(int installment) {
		this.installment = installment;
	}


	public int getRoi() {
		return roi;
	}


	public void setRoi(int roi) {
		this.roi = roi;
	}


	public int getInterestAmount() {
		return interestAmount;
	}


	public void setInterestAmount(int interestAmount) {
		this.interestAmount = interestAmount;
	}


	public double getOutsandingAmount() {
		return outsandingAmount;
	}


	public void setOutsandingAmount(double outsandingAmount) {
		this.outsandingAmount = outsandingAmount;
	}


	public LOANTYPE getLoanType() {
		return loanType;
	}


	public void setLoanType(LOANTYPE loanType) {
		this.loanType = loanType;
	}

	
}
