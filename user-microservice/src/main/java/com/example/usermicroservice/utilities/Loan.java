package com.example.usermicroservice.utilities;
public class Loan {

	private double requestedAmount;
	private int tenure;
    private LOANTYPE type;
    
    public Loan() {
    	super();
    	// TODO Auto-generated constructor stub
    }

    public Loan(double requestedAmount, int tenure, LOANTYPE type) {
		super();
		this.requestedAmount = requestedAmount;
		this.tenure = tenure;
		this.type = type;
	}
    
	public LOANTYPE getType() {
		return type;
	}
	public void setType(LOANTYPE type) {
		this.type = type;
	}
	public double getRequestedAmount() {
		return requestedAmount;
	}
	public void setRequestedAmount(double requestedAmount) {
		this.requestedAmount = requestedAmount;
	}
	public int getTenure() {
		return tenure;
	}
	public void setTenure(int tenure) {
		this.tenure = tenure;
	}

	@Override
	public String toString() {
		return "Loan [requestedAmount=" + requestedAmount + ", tenure=" + tenure + ", type=" + type + "]";
	}

   
}
