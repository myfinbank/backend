package com.example.usermicroservice.utilities;

public class RequestLoan {
	private long userId;
	private LOANTYPE loantType;
	private double annualIncome;
	private double requestedAmount;
	private int period;
	
	public RequestLoan() {
		super();
		// TODO Auto-generated constructor stub
	}

	public RequestLoan(long userId, LOANTYPE loantType, double annualIncome, double requestedAmount, int period) {
		super();
		this.userId = userId;
		this.loantType = loantType;
		this.annualIncome = annualIncome;
		this.requestedAmount = requestedAmount;
		this.period = period;
	}

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public LOANTYPE getLoantType() {
		return loantType;
	}

	public void setLoantType(LOANTYPE loantType) {
		this.loantType = loantType;
	}

	public double getAnnualIncome() {
		return annualIncome;
	}

	public void setAnnualIncome(double annualIncome) {
		this.annualIncome = annualIncome;
	}

	public double getRequestedAmount() {
		return requestedAmount;
	}

	public void setRequestedAmount(double requestedAmount) {
		this.requestedAmount = requestedAmount;
	}

	public int getPeriod() {
		return period;
	}

	public void setPeriod(int period) {
		this.period = period;
	}
	
	
}