package com.example.usermicroservice.utilities;

import java.util.List;

public class UserList {
	long userId;
	String userName;
	String email;
//	List<UserAccounts> userAccounts;
	
	public UserList() {
		super();
		// TODO Auto-generated constructor stub
	}

	public UserList(long userId, String userName, String email) {
	super();
	this.userId = userId;
	this.userName = userName;
	this.email = email;
}

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Override
	public String toString() {
		return "UserList [userId=" + userId + ", userName=" + userName + ", email=" + email + "]";
	}

	
}
