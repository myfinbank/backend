package com.example.usermicroservice.utilities;

public class FDApprovalRequest {
	private boolean isApproved;
	private String accountType;
	private int tenure;
	private double amount;
	
	
	public FDApprovalRequest() {
		super();
		// TODO Auto-generated constructor stub
	}

	public FDApprovalRequest(boolean isApproved, String accountType, int tenure, double amount) {
		super();
		this.isApproved = isApproved;
		this.accountType = accountType;
		this.tenure = tenure;
		this.amount = amount;
	}
	
	public boolean isApproved() {
		return isApproved;
	}
	public void setApproved(boolean isApproved) {
		this.isApproved = isApproved;
	}
	public String getAccountType() {
		return accountType;
	}
	public void setAccountType(String accountType) {
		this.accountType = accountType;
	}
	public int getTenure() {
		return tenure;
	}
	public void setTenure(int tenure) {
		this.tenure = tenure;
	}
	public double getAmount() {
		return amount;
	}
	public void setAmount(double amount) {
		this.amount = amount;
	}
	
	
}
