package com.example.usermicroservice.utilities;

public class UserAccounts {
	private String accountNumber;
	private ACCOUNTTYPE type;
	private double amountAvailable;
	public UserAccounts() {
		super();
		// TODO Auto-generated constructor stub
	}
	public UserAccounts(String accountNumber, ACCOUNTTYPE type, double amountAvailable) {
		super();
		this.accountNumber = accountNumber;
		this.type = type;
		this.amountAvailable = amountAvailable;
	}
	
	public String getAccountNumber() {
		return accountNumber;
	}
	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}
	public ACCOUNTTYPE getType() {
		return type;
	}
	public void setType(ACCOUNTTYPE type) {
		this.type = type;
	}
	public double getAmountAvailable() {
		return amountAvailable;
	}
	public void setAmountAvailable(double amountAvailable) {
		this.amountAvailable = amountAvailable;
	}
	@Override
	public String toString() {
		return "UserAccounts [accountNumber=" + accountNumber + ", type=" + type + ", amountAvailable="
				+ amountAvailable + "]";
	}
	
	
}
