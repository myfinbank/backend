package com.example.usermicroservice.utilities;

public enum ACCOUNTTYPE {
	SAVING,
	CURRENT,
	DEPOSITEACCOUNT,
	RECCURINGDEPOSITE,
	LOANACCOUNT
}
