package com.example.usermicroservice.utilities;

public class Payment {
	private String fromAccountNumber;
	private String toAccountNumber;
	private double amount;
	private String remark;
	private ACCOUNTTYPE type;
    
	public Payment() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Payment(String fromAccountNumber, String toAccountNumber, double amount, String remark, ACCOUNTTYPE type) {
		super();
		this.fromAccountNumber = fromAccountNumber;
		this.toAccountNumber = toAccountNumber;
		this.amount = amount;
		this.remark = remark;
		this.type = type;
	}

	public String getFromAccountNumber() {
		return fromAccountNumber;
	}

	public void setFromAccountNumber(String accountNumber) {
		this.fromAccountNumber = accountNumber;
	}

	public String getToAccountNumber() {
		return toAccountNumber;
	}

	public void setToAccountNumber(String toAccountNumber) {
		this.toAccountNumber = toAccountNumber;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public ACCOUNTTYPE getType() {
		return type;
	}

	public void setType(ACCOUNTTYPE type) {
		this.type = type;
	}

	@Override
	public String toString() {
		return "Payment [fromAccountNumber=" + fromAccountNumber + ", toAccountNumber=" + toAccountNumber + ", amount="
				+ amount + ", remark=" + remark + ", type=" + type + "]";
	}


	    
}
