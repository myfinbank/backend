package com.example.usermicroservice.utilities;

public class LoanApprovalRequest {
	private boolean isApproved;
	private String loanType;
    private double requestedAmount;
    private int tenure;
    public LoanApprovalRequest() {
		super();
		// TODO Auto-generated constructor stub
	}
	public boolean isApproved() {
		return isApproved;
	}
	public void setApproved(boolean isApproved) {
		this.isApproved = isApproved;
	}
	public String getLoanType() {
		return loanType;
	}
	public void setLoanType(String loanType) {
		this.loanType = loanType;
	}
	public double getRequestedAmount() {
		return requestedAmount;
	}
	public void setRequestedAmount(double requestedAmount) {
		this.requestedAmount = requestedAmount;
	}
	public int getTenure() {
		return tenure;
	}
	public void setTenure(int tenure) {
		this.tenure = tenure;
	}
	
}
