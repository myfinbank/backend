package com.example.usermicroservice.utilities;

public class RDUtil {
	private ACCOUNTTYPE type;
	private int tenure;
	private double amount;
	
	public RDUtil(ACCOUNTTYPE type, int tenure, double amount) {
		super();
		this.type = type;
		this.tenure = tenure;
		this.amount = amount;
	}
	public RDUtil() {
		super();
		// TODO Auto-generated constructor stub
	}
	public ACCOUNTTYPE getType() {
		return type;
	}
	public void setType(ACCOUNTTYPE type) {
		this.type = type;
	}
	public int getTenure() {
		return tenure;
	}
	public void setTenure(int tenure) {
		this.tenure = tenure;
	}
	public double getAmount() {
		return amount;
	}
	public void setAmount(double amount) {
		this.amount = amount;
	}
	
	
}
