package com.example.usermicroservice.utilities;

public class MoneyTransaction {
	private String userAccountNumber;
	private double amount;
	private String remark;
	private ACCOUNTTYPE type;
	
	public MoneyTransaction() {
		super();
		// TODO Auto-generated constructor stub
	}
	public String getUserAccountNumber() {
		return userAccountNumber;
	}
	public void setUserAccountNumber(String userAccountNumber) {
		this.userAccountNumber = userAccountNumber;
	}
	public double getAmount() {
		return amount;
	}
	public void setAmount(double amount) {
		this.amount = amount;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public ACCOUNTTYPE getType() {
		return type;
	}
	public void setType(ACCOUNTTYPE type) {
		this.type = type;
	}

    
	

}
