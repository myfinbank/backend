package com.example.usermicroservice.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.usermicroservice.entity.RecurringDeposite;

public interface RecurringDepositeRepo extends JpaRepository<RecurringDeposite, Integer> {
	
}
