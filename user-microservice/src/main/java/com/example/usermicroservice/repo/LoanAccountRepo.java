package com.example.usermicroservice.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.usermicroservice.entity.LoanAccountEntity;

public interface LoanAccountRepo extends JpaRepository<LoanAccountEntity,Integer>{
	
}
