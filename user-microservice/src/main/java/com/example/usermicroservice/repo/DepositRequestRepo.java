package com.example.usermicroservice.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.usermicroservice.entity.DepositRequestEntity;

import com.sun.tools.javac.util.List;

public interface DepositRequestRepo extends JpaRepository<DepositRequestEntity,Long>{

	List<DepositRequestEntity> findByIsApproved(boolean b);

	

}
