package com.example.usermicroservice.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.usermicroservice.entity.UserEntity;

public interface UserRepo extends JpaRepository<UserEntity,Long>{
	
    UserEntity findByUsername(String username);
	
}