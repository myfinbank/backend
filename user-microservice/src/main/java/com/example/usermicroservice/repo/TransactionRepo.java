package com.example.usermicroservice.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.usermicroservice.entity.TransactionEntity;

public interface TransactionRepo extends JpaRepository<TransactionEntity, Long>{

	List<TransactionEntity> findByUserId(long userId);

}
