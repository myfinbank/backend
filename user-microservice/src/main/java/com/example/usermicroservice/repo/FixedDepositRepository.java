package com.example.usermicroservice.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.usermicroservice.entity.FixedDepositEntity;

public interface FixedDepositRepository extends JpaRepository<FixedDepositEntity, Integer> {


}
