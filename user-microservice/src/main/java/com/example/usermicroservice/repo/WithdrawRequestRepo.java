package com.example.usermicroservice.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.usermicroservice.entity.WithdrawRequestEntity;
import com.sun.tools.javac.util.List;

public interface WithdrawRequestRepo extends JpaRepository<WithdrawRequestEntity,Long>{

	List<WithdrawRequestEntity> findByIsApproved(boolean b);

}
