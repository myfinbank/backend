package com.example.usermicroservice.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.usermicroservice.entity.LoanAccountEntity;
import com.example.usermicroservice.service.AccountServices;
import com.example.usermicroservice.service.LoanAccountServiceImpl;
import com.example.usermicroservice.utilities.Loan;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/loans")
public class LoanAccountController {
    @Autowired
    private LoanAccountServiceImpl loanService;
	@Autowired
	AccountServices service;

    @PostMapping("/calculate-emi")
    public ResponseEntity<Double> calculateEMI(@RequestBody LoanAccountEntity loan) {
        double emi = loanService.calculateEMI(loan);
        return ResponseEntity.ok(emi);
    }
    
    @GetMapping("/monthly-installment")
    public ResponseEntity<MonthlyInstallmentResponse> calculateMonthlyInstallment(@RequestBody Loan loan) {
    	double interestRate = 15.00;
        // Perform the calculation
        double monthlyInterestRate = interestRate / 100 / 12;
        double numerator = monthlyInterestRate * Math.pow(1 + monthlyInterestRate, loan.getTenure());
        double denominator = Math.pow(1 + monthlyInterestRate,loan.getTenure()) - 1;
        double monthlyInstallment = loan.getRequestedAmount() * (numerator / denominator);

        // Check if the calculation is successful
        if (!Double.isNaN(monthlyInstallment) && !Double.isInfinite(monthlyInstallment)) {
            // Create a response object with the calculated monthly installment
            MonthlyInstallmentResponse response = new MonthlyInstallmentResponse(monthlyInstallment);

            // Return the response object
            return ResponseEntity.ok(response);
        } else {
            // Return a bad request response if the calculation fails
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
        }
    }

//     @PostMapping("/request-loan")
//     public ResponseEntity<LoanAccountEntity> requestLoan(@RequestBody RequestLoan loan) {
//     if(loan!=null) {
//    	 double balance = service.currentBalance(loan.getUserId());
//    	 if(0.3*loan.getRequestedAmount()<balance) {
//    		 LoanAccountEntity loanObj = new LoanAccountEntity();
//    		 loanObj.setSancationedAmount(loan.getRequestedAmount());
//    		 loanObj.setApproved(false);
//    		 loanObj.setRequestedAmount(loan.getRequestedAmount());
//    		 loanObj.setInstallment(loan.getPeriod());
//    	 }
//     }
//     LoanAccountEntity savedLoan = loanService.saveLoan(loan);
//     return ResponseEntity.ok(savedLoan);
//     }
}
