package com.example.usermicroservice.controller;

public class LoanApprovalResponse {
    private boolean approved;

    // Constructors, getters, and setters

    public LoanApprovalResponse(boolean approved) {
        this.approved = approved;
    }

    public boolean isApproved() {
        return approved;
    }

    public void setApproved(boolean approved) {
        this.approved = approved;
    }
}

