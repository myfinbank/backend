package com.example.usermicroservice.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.usermicroservice.entity.AccountEntity;
import com.example.usermicroservice.entity.FixedDepositEntity;
import com.example.usermicroservice.service.FixedDepositService;

@RestController
@RequestMapping("/FD")
public class FixedDepositController {
	@Autowired
	 private FixedDepositService fdService;


	    @GetMapping("/account/{userId}")
	    public ResponseEntity<Optional<AccountEntity>> getAccountByUserId(@PathVariable long userId) {
	        Optional<AccountEntity> account = fdService.getAccountByUserId(userId);
	        return ResponseEntity.ok(account);
	    }

	    @PostMapping("/fixed-deposit/{userId}")
	    public ResponseEntity<String> createFixedDeposit(FixedDepositEntity entity, @PathVariable long userId) {
	        Optional<AccountEntity> account = fdService.getAccountByUserId(userId);
	        if (account.get().getAmountAvailable() >= entity.getAmount()) {
	            fdService.updateAccountBalance(userId, entity.getAmount());
	            fdService.createFixedDeposit(entity);
	            return ResponseEntity.ok("Fixed deposit created successfully.");
	        } else {
	            return ResponseEntity.badRequest().body("Insufficient balance.");
	        }
	    }

	    @GetMapping("/maturity-amount")
	    public ResponseEntity<Double> getMaturityAmount(
	    		@RequestParam int tenure,@RequestParam double amount,@RequestParam double interestRate) {
	        double maturityAmount = fdService.calculateMaturityAmount(tenure, amount, interestRate);
	        return ResponseEntity.ok(maturityAmount);
	    }
}
