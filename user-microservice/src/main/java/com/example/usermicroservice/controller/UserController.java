package com.example.usermicroservice.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.usermicroservice.entity.UserEntity;
import com.example.usermicroservice.service.AuthService;
import com.example.usermicroservice.service.UserService;
import com.example.usermicroservice.utilities.LoginInput;
import com.example.usermicroservice.utilities.UserList;
import com.example.usermicroservice.utilities.UserProfile;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
public class UserController {

    @Autowired
    private AuthService authService;
    
    @Autowired
    private UserService userService;
    
    //Login function Mapping
    
    @PostMapping("/login")
    public ResponseEntity<UserEntity> login(@RequestBody LoginInput loginInput) {
        UserEntity user = authService.login(loginInput.getUsername(), loginInput.getPassword());
        if (user == null) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
        }
        return ResponseEntity.ok(user);
    }

    // Signup function Mapping
    @PostMapping("/signup")
    public ResponseEntity<UserEntity> signup(@RequestBody UserEntity user) {
        UserEntity createdUser = authService.signup(user.getUsername(), user.getPassword(), user.getEmail(),
                user.getPhone(),
                user.getAddress());
        if (createdUser == null) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        }
        return ResponseEntity.ok(createdUser);
    }
    
    @GetMapping("/userlist")
    public ResponseEntity<List<UserList>> getUserList(){
    	System.out.println("userlist called");
    	List<UserList> usersList = userService.getAllUsers();
    	if(usersList == null) {
    		return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
    	}
    	return ResponseEntity.status(HttpStatus.OK).body(usersList);
    }

    @GetMapping("/user-profile")
    public ResponseEntity<List<UserProfile>> getUserProfiles(){
    	List<UserProfile> usersProfileList = userService.getUserProfiles();
    	if(usersProfileList == null) {
    		return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
    	}
    	return ResponseEntity.status(HttpStatus.OK).body(usersProfileList);
    }
}
