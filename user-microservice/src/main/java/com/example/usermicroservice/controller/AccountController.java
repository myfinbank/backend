package com.example.usermicroservice.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.usermicroservice.entity.AccountEntity;
import com.example.usermicroservice.entity.UserEntity;
import com.example.usermicroservice.service.AccountServices;
import com.example.usermicroservice.utilities.ACCOUNTTYPE;
import com.example.usermicroservice.utilities.Payment;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/account")
public class AccountController {
	@Autowired
	AccountServices service;

	@GetMapping("/details/{userId}")
	public ResponseEntity<List<AccountEntity>> getAccountDetails(@PathVariable long userId) {
		List<AccountEntity> accountList = service.getAllAccounts(userId);
		if (!accountList.isEmpty()) {
			return ResponseEntity.status(HttpStatus.OK).body(accountList);
		} else {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
		}
	}

	@PostMapping("/deposit")
	public ResponseEntity<AccountEntity> deposit(@RequestBody Payment paymentObj) {
		System.out.println(paymentObj.toString());
		AccountEntity updatedAccount = service.depositMoney(paymentObj);
		return service.getUpdatedAccount(updatedAccount, paymentObj);
	}

	@PostMapping("/withdraw")
	public ResponseEntity<AccountEntity> withdraw(@RequestBody Payment paymentObj) {
		AccountEntity updatedAccount = service.withdrawMoney(paymentObj);
		return service.getUpdatedAccount(updatedAccount, paymentObj);
	}
	@PostMapping("/transfer")
	public ResponseEntity<AccountEntity> transfer(@RequestBody Payment paymentObj){
		AccountEntity updatedAccount = service.transferMoney(paymentObj);
		return service.getUpdatedAccount(updatedAccount, paymentObj);
	}
	@PostMapping("/create-user-account/{type}")
	public ResponseEntity<AccountEntity> createUserAccount(@RequestBody UserEntity user, @PathVariable ACCOUNTTYPE type){
		AccountEntity createdAccount = service.createUserAccount(user,type);
		if(createdAccount!=null) {			
			return ResponseEntity.status(HttpStatus.CREATED).body(createdAccount);
		}
		return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
	}
}