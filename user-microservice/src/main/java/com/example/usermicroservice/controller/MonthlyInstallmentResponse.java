package com.example.usermicroservice.controller;

public class MonthlyInstallmentResponse {

    private double monthlyInstallment;

    public MonthlyInstallmentResponse(double monthlyInstallment) {
        this.monthlyInstallment = monthlyInstallment;
    }

    public double getMonthlyInstallment() {
        return monthlyInstallment;
    }

    public void setMonthlyInstallment(double monthlyInstallment) {
        this.monthlyInstallment = monthlyInstallment;
    }
}

