package com.example.usermicroservice.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.usermicroservice.entity.AccountEntity;
import com.example.usermicroservice.entity.RecurringDeposite;
import com.example.usermicroservice.service.RDepositeSerImplimentation;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/RD")
public class RecurringDepositeController {
	
	@Autowired
	private RDepositeSerImplimentation rDService;



    @GetMapping("/account/{userId}")
    public ResponseEntity<Optional<AccountEntity>> getAccountByUserId(@PathVariable long userId) {
        Optional<AccountEntity> account = rDService.getAccountByUserId(userId);
        return ResponseEntity.ok(account);
    }

    @PostMapping("/recurring-deposit/{userId}")
    public ResponseEntity<String> createRecurringDeposit(@RequestBody RecurringDeposite entity,@PathVariable long userId
//            @RequestParam String accountNumber,
//            @RequestParam Long amount,
//            @RequestParam double interestRate
    ) {
        Optional<AccountEntity> account = rDService.getAccountByUserId(userId);
        if (account.get().getAmountAvailable() >= entity.getAmount()) {
            rDService.updateAccountBalance(userId, entity.getAmount());
            rDService.createRecurringDeposit(entity);
            return ResponseEntity.ok("Recurring deposit created successfully.");
        } else {
            return ResponseEntity.badRequest().body("Insufficient balance.");
        }
    }

    @GetMapping("/total-savings/{userId}")
    public ResponseEntity<Double> calculateTotalSavings(int tenureMonths, double monthlyAmount, double interestRate) {
        double totalSavings = rDService.calculateTotalSavings(tenureMonths,monthlyAmount,interestRate);
        return ResponseEntity.ok(totalSavings);
    }

    
}
