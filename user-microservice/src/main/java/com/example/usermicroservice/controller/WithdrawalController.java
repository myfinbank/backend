package com.example.usermicroservice.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.usermicroservice.entity.WithdrawRequestEntity;
import com.example.usermicroservice.repo.WithdrawRequestRepo;
import com.sun.tools.javac.util.List;

@RestController
@RequestMapping("/withdrawal")
public class WithdrawalController {
	
	@Autowired
	WithdrawRequestRepo requestRepo;

    private static final double MINIMUM_BALANCE = 1000.0;
    
    @GetMapping("/withdrawRequest")
    public ResponseEntity<List<WithdrawRequestEntity>> getWithdrawlReuests() {
    	boolean isApproved;
		List<WithdrawRequestEntity> requests = requestRepo.findByIsApproved(isApproved=false);
		if(requests.length()<0)
			return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
		return ResponseEntity.status(HttpStatus.OK).body(requests);
    	
    }
    
    @PostMapping("/withdrawRequest/{requestId}")
    public ResponseEntity<WithdrawRequestEntity> approveWithdrawlReuest(@PathVariable Long requestId) {
    	WithdrawRequestEntity request=	requestRepo.findById(requestId).get();
    	
    	if (request == null) {
    		return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
    	}
    	
    	request.isApproved = true;
    	return ResponseEntity.status(HttpStatus.OK).body(request);
    		
		
    	
    }
    

    @PostMapping("/request")
    public ResponseEntity<Void> requestWithdrawal(@RequestBody WithdrawRequestEntity withdrawalRequest) {
        // Replace the logic below with your actual withdrawal processing logic

        // Example logic: Check if the withdrawal amount is within certain limits
        double withdrawalAmount = withdrawalRequest.getAmount();
        if (withdrawalAmount > 0 && withdrawalAmount <= 10000) {
            // Check if the account has sufficient balance (replace with actual logic)
            double accountBalance = getAccountBalance(withdrawalRequest.getUserAccountNumber());

            // Check if the account balance is above the minimum required
            if (accountBalance >= MINIMUM_BALANCE && accountBalance >= withdrawalAmount) {
            	withdrawalRequest.isApproved=false;
               
         requestRepo.save(withdrawalRequest);

                // Return a ResponseEntity with OK status and without a body
                return ResponseEntity.ok().build();
            } else {
                // Return a bad request response if the account balance is insufficient
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
            }
        } else {
            // Return a bad request response if the withdrawal amount is not within limits
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        }
    }

    // Replace these methods with actual methods to interact with the database or external system
    private double getAccountBalance(String accountNumber) {
        // Implement logic to retrieve the account balance from the database
        // Replace with actual database interaction
        return 5000.0; // Example balance
    }

   
}
