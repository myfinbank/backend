package com.example.usermicroservice.controller;

public class DepositResponse {

    private double depositAmount;

    // Default constructor
    public DepositResponse() {
    }

    // Constructor with deposit amount
    public DepositResponse(double depositAmount) {
        this.depositAmount = depositAmount;
    }

    // Getter and setter for depositAmount
    public double getDepositAmount() {
        return depositAmount;
    }

    public void setDepositAmount(double depositAmount) {
        this.depositAmount = depositAmount;
    }
}
