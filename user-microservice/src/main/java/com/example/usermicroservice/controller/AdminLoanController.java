package com.example.usermicroservice.controller;


import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.usermicroservice.utilities.LoanApprovalRequest;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/admin")
public class AdminLoanController {

	@PostMapping("/approve-loan")
	public ResponseEntity<LoanApprovalResponse> approveLoan(@RequestBody LoanApprovalRequest approvalRequest) {
	    // Implement your logic to approve or reject the loan based on the approvalRequest
	    // For simplicity, returning a LoanApprovalResponse object; replace with actual logic

	    LoanApprovalResponse response = new LoanApprovalResponse(approvalRequest.isApproved());

	    return ResponseEntity.ok(response);
	}


    // You can add more admin-specific endpoints as needed
}
