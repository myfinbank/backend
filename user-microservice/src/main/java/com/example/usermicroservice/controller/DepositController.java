package com.example.usermicroservice.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.usermicroservice.entity.DepositRequestEntity;
import com.example.usermicroservice.repo.DepositRequestRepo;
import com.example.usermicroservice.utilities.MoneyTransaction;
import com.sun.tools.javac.util.List;


@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/deposit")
public class DepositController {
	
	@Autowired
	DepositRequestRepo requestRepo;

    private static final double MINIMUM_BALANCE = 1000.0;
    
    @GetMapping("/depositRequest")
    public ResponseEntity<List<DepositRequestEntity>> getDepositRequests() {
    	boolean isApproved;
		List<DepositRequestEntity> requests = requestRepo.findByIsApproved(isApproved=false);
		if(requests.length()<0)
			return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
		return ResponseEntity.status(HttpStatus.OK).body(requests);
    	
    }
    
    @PostMapping("/depositRequest")
    public ResponseEntity<DepositRequestEntity> approveDepositRequest(@PathVariable Long requestId) {
    	DepositRequestEntity request=	requestRepo.findById(requestId).get();
    	if (request == null) {
    		return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
    	}
    	return ResponseEntity.status(HttpStatus.OK).body(request);
    		
		
    	
    }
    	
    @PostMapping("/request")
    public ResponseEntity<DepositResponse> requestDeposit(@RequestBody MoneyTransaction  payment) {
        // Implement your logic to process the deposit request
        // For simplicity, returning a DepositResponse; replace with actual logic

        // Example logic: Check if the deposit amount is within certain limits
        if (payment.getAmount() > 0 && payment.getAmount() <= 50000) {
            // Perform deposit and create a response object
            DepositResponse depositResponse = new DepositResponse(payment.getAmount());

            // Return a ResponseEntity with OK status and the deposit response object
            return ResponseEntity.ok(depositResponse);
        } else {
            // Return a bad request response
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
        }
    }
}

